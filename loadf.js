async function lf(client) {
  const {readdirSync} = require("fs")
  const funcFiles = readdirSync('./funcs').filter(file => file.endsWith('.js'));

for (const file of funcFiles) {
  const func = require(`./funcs/${file}`);
  client.funcs.set(func.name, func)

  console.log(`${func.name} загружено.`)
}
}
module.exports = {lf}