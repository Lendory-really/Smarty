module.exports = {
  name: "exec",
  run: async(client,message) => {
    try {
      let prefix = "c!"
      if (message.author.bot) return;
      if (!message.guild) return;
      if (!message.content.startsWith(prefix)) return;
  
      if (!message.member) message.member = await message.guild.fetchMember(message);
  
      const args = message.content.slice(prefix.length).trim().split(/ +/g);
      const cmd = args.shift().toLowerCase();
      
      if (cmd.length === 0) return;
      
      let command = client.commands.get(cmd);
      if (!command) command = client.commands.get(client.aliases.get(cmd));
      try {
        if (command) 
            command.run(client, message, args);
      } catch(e) {
        const embed = {
          title: "Обнаружена ошибка",
          description: `\`\`\`js\n${e.name}: ${e.message}\`\`\``,
          color: "fc0000"
        }
        message.reply({embeds:[embed]})
      }
  } catch (e) {
    console.log(`${e.name}: ${e.message}`)
    message.reply("Извините, но произашла неизвестная ошибка, попробуйте еще раз\n**Если это не помогло, напишите Lendory#5197**")
  }
  }
}