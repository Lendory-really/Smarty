module.exports = {
  name: "ldb",
  run: async() => {
    global.mongoose = require('mongoose')
    const dataURL = process.env.URL
    global.Guild = require("../data/guild.js");
    global.User = require('../data/user.js');
    global.Guser = require('../data/guser.js') 
    mongoose.connect(dataURL, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    }).then(() => {
      console.log("Датабаза подключена")
      state = "Подключена"
    }).catch((err) => {
      console.log(err)
      state = "Ошибка"
    });
  }
}