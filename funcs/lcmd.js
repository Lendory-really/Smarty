let {MessageEmbed} = require("discord.js")

module.exports = {
  name: "lcmd",
  run: async(client,message) => {
    let a = 0
    let b = 0
    const { readdirSync } = require("fs");
    
    readdirSync("./commands/").forEach(dir => {
            const commands = readdirSync(`./commands/${dir}/`).filter(file => file.endsWith(".js"));
      //але, в кансоли
              for (let file of commands) {
                let pull = require(`../commands/${dir}/${file}`);
        
                if (pull.name) {
                    client.commands.set(pull.name, pull);
                    a += 1
                } else {
                    b += 1
                    continue;
                }
        
                if (pull.aliases && Array.isArray(pull.aliases)) pull.aliases.forEach(alias => client.aliases.set(alias, pull.name));
            }
          
        });
    message.reply(`Удалось Загрузить: ${a} команд\nНе удалось загрузить: ${b} команд`)
  }
}