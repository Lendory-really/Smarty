module.exports = {
  name: "atdb",
  run: async(message) => {
    if (message.author.bot == true) return;
    global.user = await User.findOne({ guildID: message.guild.id, userID: message.author.id });
    global.guild = await Guild.findOne({ guildID: message.guild.id });
    global.guser = await Guser.findOne({ userID: message.author.id });
    if(!user) {
      User.create({ guildID: message.guild.id, userID: message.author.id });
      console.log(`[✅ DataBase] ${message.author.username} Успешно был(а) добавлен в базу-данных`)
    }
    if(!guild) { 
      Guild.create({ guildID: message.guild.id }); 
      console.log(`[✅ DataBase] ${message.guild.name} Успешно была добавлен в базу-данных`)
    }
    if(!guser) {
      Guser.create({ userID: message.author.id })
      console.log(`${message.author.username} Успешно был(а) добавлен в глобальную базу-данных`)
    }
  }
}