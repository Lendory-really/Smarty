module.exports = {
  name: "help",
  run: async(client, cat) => {
    let cmds = client.commands
    let a = []
    cmds.forEach((value) => {
        if (value.category == cat) {
            a.push(`${value.name}`)
       }
    })
    return a.join(", ")
  }
}