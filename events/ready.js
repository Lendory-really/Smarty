module.exports = {
	name: 'ready',
	once: true,
	execute(client) {
    try {
  		console.log(`Готова: ${client.user.tag}`);
      client.user.setPresence({ activities: [{ name: 'Разработчик: Lendory#5197', type: "WATCHING" }], status: 'online' })
      
    } catch(e) {
      console.log(e)
    }
  }
}