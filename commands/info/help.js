const {MessageEmbed} = require("discord.js")
let a = []
module.exports = {
  name: "help",
  usage: "c!help [cmd]",
  description: "Выдает информацию о командах бота",
  category: "Информация",
  run: async(client,message,args) => {
    if (!args[0]) {
      const commands = await client.commands;

      let emx = new MessageEmbed()
        .setTitle("Команды")
        .setDescription(`Спасибо за помощь - \`${client.users.cache.get('887303819300577291').tag}\``)
        .setColor("#8C52FF")
        .setThumbnail(client.user.displayAvatarURL());

      let com = {};
      for (let comm of commands.values()) {
        let category = comm.category || "Категории нет";
        let name = comm.name;

        if (!com[category]) {
          com[category] = [];
        }
        com[category].push(name);
      }
      for(const [key, value] of Object.entries(com)) {
        
        let category = key;
        let cmd = value.sort()
        let desc = `\`\`\`${cmd.join(", ")}\`\`\``
        if (category == "Разработчик") {
          a.push("che")
        } else {
          emx.addField(`${category} [${value.length}]`, desc);
        }
      }
      return message.reply({embeds: [emx]});
    } else {
      let al;
      let cat;
      let us;
      let desc;
      let err = client.funcs.get("error")
      let command = client.commands.get(args[0]) || client.commands.get(client.aliases.get(args[0]))
      let eembed = {
          title: "Ошибка",
          description: "Такой команды нет",
          color: "#8C52FF"
      }
      if(!command) {
        message.reply({embeds:[eembed]});
      } else {
        al = command.aliases
        if(!command.aliases) al = "Псевдонимов нет"
        cat = command.category
        if(!command.category) cat = "Категории нет"
        desc = command.description
        if(!command.description) desc = "Информации нет"
        us = command.usage
        if(!command.usage) us = "?"
        let embed = {
          title: `Команда: ${args[0]}`,
          fields: [
            {
              name: "О команде",
              value: `\`\`\`${desc}\`\`\``
            },
            {
              name: "Категория",
              value: `\`\`\`${cat}\`\`\``,
              inline: true
            },
            {
              name: "Псевдонимы",
              value: `\`\`\`${al}\`\`\``,
              inline: true
            },
            {
              name: "Использование",
              value: `\`\`\`${us}\`\`\``,
              inline: true
            }
          ],
          color: "#8C52FF"
        }
        message.reply({embeds:[embed]})
      }
    }
  }
}