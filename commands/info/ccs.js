module.exports = {
  name: "ccs",
  category: "Информация",
  usage: "c!ccs",
  description: "Выдает информацию о командах",
  run: async(client,message,args) => {
    let cmds = client.cmds
    let a = ""
    cmds.forEach((value) => {
        a = a + `\n**${value.name}**: ${value.st}`
    })

    let embed = {
      title: "Информация",
      description: a,
      color: "#8C52FF"
    }
    message.reply({
      embeds: [embed]
    })
  }
}