const ms = require("ms")
const { MessageActionRow, MessageButton, MessageEmbed } = require('discord.js');
module.exports = {
  name: "work",
  aliases: ["w"],
  category: "Экономика",
  description: "Получение денег каждые 5 минут",
  usage: "c!work",
  run: async (client, message, args) => {
    let user = await User.findOne({ guildID: message.guild.id, userID: message.author.id });
    let embed;
    let embed1;
    if (user.stamina == 10 || user.stamina == 5 || user.stamina == 15) {
      let stembed = {
        title: "Вы устали",
        description: "Пропишите **c!rest** чтобы отдохнуть",
        color: "8C52FF"
      }
      message.reply({embeds: [stembed]})
   } else if(user.stamina <= 0) {
      let stembed = {
        title: "Вы устали",
        description: "Пропишите **c!rest** чтобы отдохнуть",
        color: "8C52FF"
      }
      message.reply({embeds: [stembed]})
    } else if(user.workcd+300000>Date.now()) {
      let cdembed = {
              title: "⌛ Задержка",
              description: `Пожалуйста подождите \`${ms(300000-(Date.now()-user.workcd))}\``,
              color: "#8C52FF"
      }
      message.reply({embeds: [cdembed]});
    } else {
      
  
      let works = ["рыбак", "завод", "шахтер", "продавец", "дальнобойщик"]
      let random = works[Math.floor((Math.random() * works.length))]
      if (random == "завод") {
        const zavod = new MessageActionRow()
        .addComponents(
          new MessageButton()
            .setCustomId('Выгрузить')
            .setLabel('Выгрузить заказ')
            .setStyle('PRIMARY'),
          )
        const filter = i => i.customId === 'Выгрузить' && i.user.id === message.author.id;
        const collector = message.channel.createMessageComponentCollector({ filter, time: 30000 });
        embed = {
          title: "Завод",
          description: "Вы работаете на заводе\nНажмите кнопку чтобы создать изделие",
          color: "#8C52FF"
        }
        let msg = await message.reply({
          embeds: [embed],
          components: [zavod]
        })
        collector.on('collect', async i => {
        	if (i.customId === 'Выгрузить') {
            let coins;
            let f = [
              "Большое",
              "Среднее",
              "Маленькое"
            ]
            let rand = f[Math.floor((Math.random() * f.length))]
            if (rand == "Большое") coins = Math.floor((Math.random() * (300-200+1)+200))
            if (rand == "Среднее") coins = Math.floor((Math.random() * (200-100+1)+100))
            if (rand == "Маленькое") coins = Math.floor((Math.random() * 100+1))
            user.stamina -= 20
            embed1 = {
              title: "Завод",
              description: `Вы успешно создали **${rand}** изделие, и вам заплатили **${coins}₽**\nВаш уровень стамины равен: ${user.stamina}`,
              color: "#8C52FF"
            }
        		await msg.edit({ 
              embed: [embed1], 
              components: []
            });
            user.money += coins
            user.save()
        	}
        });
        collector.on('end', async collected => {
          if (collected.size == 0) {
            const fish = new MessageActionRow()
              .addComponents(
                  new MessageButton()
                    .setCustomId('Продать')
                    .setLabel('Вы не успели')
                    .setStyle('DANGER')
                    .setDisabled(true)
              );
            await msg.edit({
              embeds: [embed],
              components: [fish]
            })
          } else return;
        })
      } else if (random == "рыбак"){
        const fish = new MessageActionRow()
        .addComponents(
          new MessageButton()
            .setCustomId('Продать')
            .setLabel('Продать рыбу')
            .setStyle('PRIMARY'),
          )
        const filter = i => i.customId === 'Продать' && i.user.id === message.author.id;
        const collector = message.channel.createMessageComponentCollector({ filter, time: 30000 });
        embed = {
          title: "Озеро",
          description: "Вы работаете на озере\nНажмите кнопку чтобы продать рыбу",
          color: "#8C52FF"
        }
        let msg = await message.reply({
          embeds: [embed],
          components: [fish]
        })
        collector.on('collect', async i => {
        	if (i.customId === 'Продать') {
            let coins;
            let f = [
              "Большую",
              "Среднюю",
              "Маленькую"
            ]
            let rand = f[Math.floor((Math.random() * f.length))]
            if (rand == "Большую") coins = Math.floor((Math.random() * (300-200+1)+200))
            if (rand == "Среднюю") coins = Math.floor((Math.random() * (200-100+1)+100))
            if (rand == "Маленькую") coins = Math.floor((Math.random() * 100+1))
            user.stamina -= 20
            embed1 = {
              title: "Озеро",
              description: `Вы успешно продали **${rand}** рыбу, и вам дали **${coins}₽**\nВаш уровень стамины равен: ${user.stamina}`,
              color: "#8C52FF"
            }
        		await msg.edit({ 
              embeds: [embed1],
              components: []
            });
            user.money += coins
            user.save()
        	}
        });
        collector.on('end', async collected => {
          if (collected.size == 0) {
            const fish = new MessageActionRow()
              .addComponents(
                  new MessageButton()
                    .setCustomId('Продать')
                    .setLabel('Вы не успели')
                    .setStyle('DANGER')
                    .setDisabled(true)
              );
            await msg.edit({
              embeds: [embed],
              components: [fish]
            })
          } else return;
        })
      } else if (random == "шахтер"){
        const fish = new MessageActionRow()
        .addComponents(
          new MessageButton()
            .setCustomId('Продать')
            .setLabel('Продать руду')
            .setStyle('PRIMARY'),
          )
        const filter = i => i.customId === 'Продать' && i.user.id === message.author.id;
        const collector = message.channel.createMessageComponentCollector({ filter, time: 30000 });
        embed = {
          title: "Шахта",
          description: "Вы работаете в шахте\nНажмите кнопку чтобы продать руду",
          color: "#8C52FF"
        }
        let msg = await message.reply({
          embeds: [embed],
          components: [fish]
        })
        collector.on('collect', async i => {
        	if (i.customId === 'Продать') {
            let coins;
            let f = [
              "Золото",
              "Железо",
              "Медь"
            ]
            let rand = f[Math.floor((Math.random() * f.length))]
            if (rand == "Золото") coins = Math.floor((Math.random() * (300-200)+200+1))
            if (rand == "Железо") coins = Math.floor((Math.random() * (200-100)+100+1))
            if (rand == "Медь") coins = Math.floor((Math.random() * 100+1))
            user.stamina -= 20
            embed1 = {
              title: "Шахта",
              description: `Вы успешно продали **${rand}**, и вам дали **${coins}₽**\nВаш уровень стамины равен: ${user.stamina}`,
              color: "#8C52FF"
            }
        		await msg.edit({ 
              embeds: [embed1], 
              components: []
            });
            user.money += coins
            user.save()
        	}
        });
        collector.on('end', async collected => {
          if (collected.size == 0) {
            const fish = new MessageActionRow()
              .addComponents(
                  new MessageButton()
                    .setCustomId('Продать')
                    .setLabel('Вы не успели')
                    .setStyle('DANGER')
                    .setDisabled(true)
              );
            await msg.edit({
              embeds:[embed],
              components: [fish]
            })
          } else return;
        })
      } else if (random == "продавец"){
        const fish = new MessageActionRow()
        .addComponents(
          new MessageButton()
            .setCustomId('Продать')
            .setLabel('Продать товар')
            .setStyle('PRIMARY'),
          )
        const filter = i => i.customId === 'Продать' && i.user.id === message.author.id;
        const collector = message.channel.createMessageComponentCollector({ filter, time: 30000 });
        embed = {
          title: "Магазин",
          description: "Вы работаете в магазине\nНажмите кнопку чтобы продать товар",
          color: "#8C52FF"
        }
        let msg = await message.reply({
          embeds:[embed],
          components: [fish]
        })
        collector.on('collect', async i => {
        	if (i.customId === 'Продать') {
            let coins;
            let f = [
              "Большую партию",
              "Среднюю партию",
              "Маленькую партию"
            ]
            let rand = f[Math.floor((Math.random() * f.length))]
            if (rand == "Большую партию") coins = Math.floor((Math.random() * (300-200)+200+1))
            if (rand == "Среднюю партию") coins = Math.floor((Math.random() * (200-100)+100+1))
            if (rand == "Маленькую партию") coins = Math.floor((Math.random() * 100+1))
            user.stamina -= 20
            embed1 = {
              title: "Магазин",
              description: `Вы успешно продали **${rand}** товара, и вам заплатили **${coins}₽**\nВаш уровень стамины равен: ${user.stamina}`,
              color: "#8C52FF"
            }
        		await msg.edit({ 
              embeds:[embed1], 
              components: []
            });
            user.money += coins
            user.save()
        	}
        });
        collector.on('end', async collected => {
          if (collected.size == 0) {
            const fish = new MessageActionRow()
              .addComponents(
                  new MessageButton()
                    .setCustomId('Продать')
                    .setLabel('Вы не успели')
                    .setStyle('DANGER')
                    .setDisabled(true)
              );
            await msg.edit({
              embeds:[embed],
              components: [fish]
            })
          } else return;
        })
      } else if (random == "дальнобойщик"){
        const fish = new MessageActionRow()
        .addComponents(
          new MessageButton()
            .setCustomId('Перевезти')
            .setLabel('Перевезти груз')
            .setStyle('PRIMARY'),
          )
        const filter = i => i.customId === 'Перевезти' && i.user.id === message.author.id;
        const collector = message.channel.createMessageComponentCollector({ filter, time: 30000 });
        embed = {
          title: "Дальнобойщик",
          description: "Вы работаете дальнобойщиком\nНажмите кнопку чтобы перевезти груз",
          color: "#8C52FF"
        }
        let msg = await message.reply({
          embeds:[embed],
          components: [fish]
        })
        collector.on('collect', async i => {
        	if (i.customId === 'Перевезти') {
            let coins;
            let f = [
              "Большое",
              "Среднее",
              "Маленькое"
            ]
            let rand = f[Math.floor((Math.random() * f.length))]
            if (rand == "Большое") coins = Math.floor((Math.random() * (300-200+1)+200))
            if (rand == "Среднее") coins = Math.floor((Math.random() * (200-100+1)+100))
            if (rand == "Маленькое") coins = Math.floor((Math.random() * 100+1))
            user.stamina -= 20
            embed1 = {
              title: "Дальнобойщик",
              description: `Вы успешно перевезли **${rand}** кол-во груза, и вам заплатили **${coins}₽**\nВаш уровень стамины равен: ${user.stamina}`,
              color: "#8C52FF"
            }
        		await msg.edit({ 
              embeds:[embed1], 
              components: []
            });
            user.money += coins
            user.save()
        	}
        });
        collector.on('end', async collected => {
          if (collected.size == 0) {
            const fish = new MessageActionRow()
              .addComponents(
                  new MessageButton()
                    .setCustomId('Продать')
                    .setLabel('Вы не успели')
                    .setStyle('DANGER')
                    .setDisabled(true)
              );
            await msg.edit({
              embeds:[embed],
              components: [fish]
            })
          } else return;
        })
      }
      user.workcd = Date.now()
    }
  }
}