module.exports = {
  name: "profile",
  aliases: ["p"],
  category: "Экономика",
  description: "Выдает профиль юзера",
  usage: "c!profile [user]",
  run: async (client, message, args) => {
    let user;
        if (args[0]) {
          let member = message.guild.members.cache.get(args[0].replace(/[\\<>@!]/g,"")) || message.member
          user = member
        } else {
          user = message.member
        }
    const us = await User.findOne({userID:user.id,guildID:message.guild.id})
    const gus = await User.findOne({userID:user.id})
    let avatar = user.user.avatarURL({size: 2048, dynamic: true})
    let bzn;
    let szn;
    let suzn = us.znachki
    let gzn = gus.znachki
    bzn = gzn.join(", ")
    if (!gus.znachki[0]) {
      bzn = "Пусто"
    } else { 
      bzn = gzn.join(", ")
    }
    
    
    if (!us.znachki[0]) {
      szn = "Пусто"
    } else {
      szn = suzn.join(", ")
    }
    let embed = {
      author: {
        name: `${user.user.tag}`,
        icon_url: avatar
      },
      fields: [
        {
          name: "Экономика",
          value: `**Баланс:** ${us.money}\n**Стамина:** ${us.stamina}`
        },
        {
          name: "Значки",
          value: `**Значки в боте:** ${bzn}\n**Значки на сервере:** ${szn}`
        }
      ]
    }
    message.reply({
      embeds: [embed]
    })
  
  }
}