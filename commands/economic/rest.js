const ms = require("ms")
module.exports = {
  name: "rest",
  category: "Экономика",
  usage: "c!rest",
  description: "Отдых",
  run: async(client,message,args) => {
    let user = await User.findOne({ guildID: message.guild.id, userID: message.author.id });
    if (user.stamina == 100) {
      let stembed = {
        title: "Вы уже полны сил",
        description: `Уровень стамины и так **100**, зачем вам еще?`,
        color: "#8C52FF"
      }
      message.reply({embeds: [stembed]});
    } else if (user.restcd+15000>Date.now()) {
      let cdembed = {
              title: "⌛ Задержка",
              description: `Пожалуйста подождите \`${ms(15000-(Date.now()-user.restcd))}\``,
              color: "#8C52FF"
      }
      message.reply({embeds: [cdembed]});
    } else {
    
      let embed = {
        title: "Отдых",
        description: "Вы отдохнули, и получили 5 стамины!",
        color: "#8C52FF"
      }
      message.reply({embeds: [embed]});
      user.stamina += 5
      user.restcd = Date.now()
      user.save()
    }
  }
}