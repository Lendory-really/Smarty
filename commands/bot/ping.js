module.exports = {
  name: "ping",
  aliases: ["latency"],
  usage: "c!ping",
  description: "Выдает пинг бота и пинг Discord API",
  category: "Бот",
  run: async (client,message,args) => {
    let guild = await Guild.findOne({ guildID: message.guild.id });
    const embed = {
      title: "Понг!",
      fields: [
        {name:"Пинг бота", value:`\`\`\`${Date.now()-message.createdTimestamp}ms\`\`\``,inline:true},
        {name:"Пинг Discord API", value:`\`\`\`${client.ws.ping}ms\`\`\``,inline:true}
      ],
      color: guild.color
    }
    message.reply({embeds:[embed]})
  }
}