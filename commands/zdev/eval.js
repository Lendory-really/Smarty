module.exports = {
  name: "eval",
  aliases: ["e", "ev"],
  category: "Разработчик",
  description: "Выдает результат выполненого кода",
  usage: "c!eval",
  run: async (client, message, args) => {
    if (cfg.devs.includes(message.author.id) == false) return;
  const { inspect } = require('util');
  const { MessageEmbed } = require('discord.js');

  let code = args.join(" ")
  try {
    let preEval = process.hrtime.bigint();
    let evaled = await eval(code);
    let lastEval = process.hrtime.bigint();
  if (typeof evaled !== "string") evaled = inspect(evaled);
    if (evaled.includes(process.env.TOKEN) == true) {
      let evalerror = {
        title: "Ошибочка вышла",
        description: "\`\`\`js\nTi Eblan Error: client.token is secret информация\n\`\`\`",
        color: "#8C52FF"
    }
    message.reply({embeds:[evalerror]});
    } else if (evaled.includes(process.env.URL) == true) {
      let evalerror = {
        title: "Ошибочка вышла",
        description: "\`\`\`js\nTi Eblan Error: client.db.URL is secret информация\n\`\`\`",
        color: "#8C52FF"
    }
    message.reply({embeds:[evalerror]});
    } else {
      message.reply(`\`\`\`js\n${evaled.slice(0, 1900)}\`\`\``, { code: "js" });
    }
  } catch(e) {
    if (typeof(e) == "string") e = e.replace(/`/g, "`" + String.fromCharCode(8203)).replace(/@/g, "@" + String.fromCharCode(8203));
    let evalerror = {
        title: "Ошибочка вышла",
        description: "\`\`\`js\n" + e + "\n\`\`\`",
        color: "#8C52FF"
    }
    message.reply({embeds:[evalerror]});
  }
}}