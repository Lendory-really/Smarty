const schema = mongoose.Schema({
    guildID: String,
    color: {
      type: String,
      default: "#8C52FF"
    }
})
module.exports = mongoose.model("Guild", schema)