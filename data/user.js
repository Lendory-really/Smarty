const schema = mongoose.Schema({
    
    userID: String,
    guildID: String,
    money: {
      type: Number, 
      default:0
    },
    workcd: {
      type: Number,
      default:0
    },
    stamina: {
      type: Number,
      default:100
    },
    restcd: {
      type: Number,
      default: 0
    },
    znachki: {
      type: Array,
      default: []
    }

},{
  versionKey: false
})
module.exports = mongoose.model("User", schema)